This code is my first project to print and preview daily expenses and earning. It can be used to manage one's daily accounts.
    
    print("Welcome to PiggyBank")
    print("This program is designed to help you view your account transactions.\n")
    #Balance will store the amout of money in piggy bank
    balance=-1
    lt=0
    startover=1
    transactionHistory=[] #Determines whether to continue after reaching the end
    
    def deposit(v):
        global balance
        global lt
        balance = balance+v
        lt = v
    def withdraw(v):
        global balance
        global lt
        if v <= balance:
            balance -= v
            lt = -1*v
        else:
            print("Insufficient Balance")
    def statement():
        print("Your current balance is: ", balance)
        print("Last Transaction: ", lt )
        print("\n")
        
    while balance<0:
        balance=int(input("Kindly enter your PiggyBank balance: "))
        if balance<0:
            print("Enter valid balance.")
            print("Your current balance is ", balance, "\n")
    
    while startover==1:
        
        userInput="a"
        while userInput!="W" and userInput!="D":
            userInput=(input("What transactions do you want to perform, withdrawal(W) or deposit(D): "))
            if userInput != "W" and userInput != "D":
                print("Kindly input valid type of transaction, (W or D).")
    
        currentTransaction = float(input("Kindly enter the amout of money that you want to transact: "))
    
        while currentTransaction<=0:
            print("Kindly input valid trnsaction amount.\n" )
            
        if userInput=="W":        
            withdraw(currentTransaction)
            transactionHistory.append(-currentTransaction)
            print("Your current account stats is as follows:")
            statement()
            
        elif userInput == "D":
            deposit(currentTransaction)
            transactionHistory.append(currentTransaction)
            print("Your current account stats is as follows:")
            statement()
                    
    
        x="a"
        while x!="Y" and x!="N":
            x=str(input("Do you want to carry out another transaction(Y/N)? "))
        
        if x=="N":
            startover=0
            
    print("Your final transaction history for last 10 is as follows: \n" )
    transactionHistory.reverse()
    i=0
    for transaction in transactionHistory:
        i+=1
        if i<=10:
            if transaction<0:
                print("Amount withdrawn: ", transaction)
            elif transaction>0:
                print("Amount deposited: ", transaction)
    
    print("\n This is the end of your account statement. \n Hope you are satisfied :). \n Thank You for using this")     

Suggestions to this code are welcome.
    
